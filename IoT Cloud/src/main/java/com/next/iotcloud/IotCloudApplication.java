package com.next.iotcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(IotCloudApplication.class, args);
    }

}
