package com.next.iotcloud.repositoriesJDBC.contracts;

import com.next.iotcloud.models.SensorDetails;

import java.util.List;

public interface SensorDetailsRepository {

    void createSensorDetails(SensorDetails sensorDetails);

    void updateSensorDetails(SensorDetails sensorDetails);

    void deleteSensorDetails(SensorDetails sensorDetails);

    SensorDetails getById(int id);

    List<SensorDetails> getAll();

    SensorDetails getByName(String name);
}
