package com.next.iotcloud.repositoriesJDBC;

import com.next.iotcloud.models.DataTypeString;
import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositoriesJDBC.contracts.DataTypeStringRepository;
import com.next.iotcloud.repositoriesJDBC.contracts.SensorDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class DataTypeStringRepositoryImpl implements DataTypeStringRepository {

    private String databaseURL, databaseUsername, databasePassword;
    private SensorDetailsRepository sensorDetailsRepository;

    @Autowired
    public DataTypeStringRepositoryImpl(Environment environment, SensorDetailsRepository sensorDetailsRepository) {
        databaseURL = environment.getProperty("spring.datasource.url");
        databaseUsername = environment.getProperty("spring.datasource.username");
        databasePassword = environment.getProperty("spring.datasource.password");
        this.sensorDetailsRepository = sensorDetailsRepository;
    }

    @Override
    public void createDataTypeString(DataTypeString dataTypeString, SensorDetails sensorDetails) {
        String sql = "insert into data_type_string(id, data, effective_date, sensor_id) values (?, ?, current_timestamp, ?)";

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setLong(1, dataTypeString.getId());
            statement.setString(2, dataTypeString.getData());
            statement.setLong(3, sensorDetails.getId());
            int createdRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateDataTypeString(DataTypeString dataTypeString, SensorDetails sensorDetails) {
        String sql = "update data_type_string set data = ?, effective_date = current_timestamp, sensor_id = ? where id = ?";

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setString(1, dataTypeString.getData());
            statement.setLong(2, sensorDetails.getId());
            statement.setLong(3, dataTypeString.getId());
            int updatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteDataTypeString(DataTypeString dataTypeString) {
        String query = "delete from data_type_string where id = ?";
        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(query);
        ) {
            statement.setLong(1, dataTypeString.getId());
            int deletedRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DataTypeString getById(int id) {
        String sql = "select * from data_type_string where id = ?";

        List<DataTypeString> data = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    SensorDetails sensorDetails = sensorDetailsRepository.getById(resultSet.getInt("sensor_id"));
                    DataTypeString dataTypeString = new DataTypeString(resultSet.getLong("data_type_string_id"), resultSet.getString("data"), resultSet.getTimestamp("effective_date"), sensorDetails);
                    data.add(dataTypeString);
                }
            }
            return data.get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DataTypeString> getAll() {
        String sql = "select * from data_type_string";

        List<DataTypeString> data = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)
        ) {
            while (resultSet.next()) {
                SensorDetails sensorDetails = sensorDetailsRepository.getById(resultSet.getInt("sensor_id"));
                DataTypeString dataTypeString = new DataTypeString(resultSet.getLong("data_type_string_id"),
                        resultSet.getString("data"),
                        resultSet.getTimestamp("effective_date"), sensorDetails);
                data.add(dataTypeString);
            }
            return data;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
