package com.next.iotcloud.repositoriesJDBC;

import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositoriesJDBC.contracts.SensorDetailsRepository;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class SensorDetailsRepositoryImpl implements SensorDetailsRepository {

    private String databaseURL, databaseUsername, databasePassword;

    public SensorDetailsRepositoryImpl(Environment environment) {
        databaseURL = environment.getProperty("spring.datasource.url");
        databaseUsername = environment.getProperty("spring.datasource.username");
        databasePassword = environment.getProperty("spring.datasource.password");
    }

    @Override
    public void createSensorDetails(SensorDetails sensorDetails) {
        String sql = "insert into sensor_details(id, sensor_name) values (?, ?)";

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setLong(1, sensorDetails.getId());
            statement.setString(2, sensorDetails.getSensorName());
            int createdRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateSensorDetails(SensorDetails sensorDetails) {
        String sql = "update sensor_details set sensor_name = ? where id = ?";

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setString(1, sensorDetails.getSensorName());
            statement.setLong(2, sensorDetails.getId());
            int updatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteSensorDetails(SensorDetails sensorDetails) {
        String query = "delete from sensor_details where id = ?";
        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(query);
        ) {
            statement.setLong(1, sensorDetails.getId());
            int deletedRows = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SensorDetails getById(int id) {
        String sql = "select * from sensor_details where id = ?";

        List<SensorDetails> sensors = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    SensorDetails sensorDetails = new SensorDetails(resultSet.getLong("sensor_details_id"), resultSet.getString("sensor_name"));
                    sensors.add(sensorDetails);
                }
            }
            return sensors.get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SensorDetails> getAll() {
        String sql = "select * from sensor_details";

        List<SensorDetails> sensors = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)
        ) {
            while (resultSet.next()) {
                SensorDetails sensorDetails = new SensorDetails(resultSet.getLong("sensor_details_id"), resultSet.getString("sensor_name"));
                sensors.add(sensorDetails);
            }
            return sensors;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SensorDetails getByName(String name) {
        String sql = "select * from sensor_details where sensor_name = ?";

        List<SensorDetails> sensors = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(databaseURL, databaseUsername, databasePassword);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    SensorDetails sensorDetails = new SensorDetails(resultSet.getLong("sensor_details_id"),
                            resultSet.getString("sensor_name"));
                    sensors.add(sensorDetails);
                }
            }
            return sensors.get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
