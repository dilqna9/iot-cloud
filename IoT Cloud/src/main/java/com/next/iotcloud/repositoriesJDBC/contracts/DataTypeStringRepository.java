package com.next.iotcloud.repositoriesJDBC.contracts;

import com.next.iotcloud.models.DataTypeString;
import com.next.iotcloud.models.SensorDetails;

import java.util.List;

public interface DataTypeStringRepository {

    void createDataTypeString(DataTypeString dataTypeString, SensorDetails sensorDetails);

    void updateDataTypeString(DataTypeString dataTypeString, SensorDetails sensorDetails);

    void deleteDataTypeString(DataTypeString dataTypeString);

    DataTypeString getById(int id);

    List<DataTypeString> getAll();
}
