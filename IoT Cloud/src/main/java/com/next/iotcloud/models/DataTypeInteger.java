package com.next.iotcloud.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DataTypeInteger {

    private Long id;
    private int data;
    private Date effectiveDate;
    private SensorDetails sensorDetails;

    public DataTypeInteger() {
    }

    public DataTypeInteger(Long id, int data, Date effectiveDate, SensorDetails sensorDetails){
        this.id = id;
        this.data = data;
        this.effectiveDate = effectiveDate;
        this.sensorDetails = sensorDetails;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public int getData() {
        return data;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    @ManyToOne
    @JoinColumn(name = "sensor_id", nullable = false)
    public SensorDetails getSensorDetails() {
        return sensorDetails;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setSensorDetails(SensorDetails sensorDetails) {
        this.sensorDetails = sensorDetails;
    }
}
