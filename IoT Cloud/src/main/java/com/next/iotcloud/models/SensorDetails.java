package com.next.iotcloud.models;

import javax.persistence.*;

@Entity
public class SensorDetails {

    private Long id;
    private String sensorName;

    public SensorDetails() {
    }

    public SensorDetails(Long id, String sensorName) {
        this.id = id;
        this.sensorName = sensorName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }
}
