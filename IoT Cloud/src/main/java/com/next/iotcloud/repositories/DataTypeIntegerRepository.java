package com.next.iotcloud.repositories;

import com.next.iotcloud.models.DataTypeInteger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataTypeIntegerRepository extends JpaRepository<DataTypeInteger, Long> {

    DataTypeInteger findById(int id);

    List<DataTypeInteger> findAll();

    List<DataTypeInteger> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeInteger> findAllBySensorDetailsSensorName(String sensorName);

}
