package com.next.iotcloud.repositories;

import com.next.iotcloud.models.DataTypeBoolean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataTypeBooleanRepository  extends JpaRepository<DataTypeBoolean, Long> {

    DataTypeBoolean findById(int id);

    List<DataTypeBoolean> findAll();

    List<DataTypeBoolean> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeBoolean> findAllBySensorDetailsSensorName(String sensorName);

}
