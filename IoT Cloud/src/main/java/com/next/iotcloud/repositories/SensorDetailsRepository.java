package com.next.iotcloud.repositories;

import com.next.iotcloud.models.SensorDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorDetailsRepository extends JpaRepository<SensorDetails, Long> {

    SensorDetails findById(int id);

    List<SensorDetails> findAll();

    SensorDetails findBySensorName(String name);

}
