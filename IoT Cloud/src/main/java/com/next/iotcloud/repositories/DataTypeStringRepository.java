package com.next.iotcloud.repositories;

import com.next.iotcloud.models.DataTypeString;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataTypeStringRepository extends JpaRepository<DataTypeString, Long> {

    DataTypeString findById(int id);

    List<DataTypeString> findAll();

    List<DataTypeString> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeString> findAllBySensorDetailsSensorName(String sensorName);

}
