package com.next.iotcloud.repositories;

import com.next.iotcloud.models.DataTypeDouble;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataTypeDoubleRepository extends JpaRepository<DataTypeDouble, Integer> {

    DataTypeDouble findById(Long id);

    List<DataTypeDouble> findAll();

    List<DataTypeDouble> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeDouble> findAllBySensorDetailsSensorName(String sensorName);

}
