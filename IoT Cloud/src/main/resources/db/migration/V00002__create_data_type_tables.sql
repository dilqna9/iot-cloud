create table data_type_string
(
    id             bigserial not null,
    data           text      not null,
    effective_date timestamp not null,
    sensor_id      int8      not null,
    constraint data_type_string_pkey primary key (id),
    constraint data_type_string_sensor_details_fkey foreign key (sensor_id) references sensor_details (id)

);

create table data_type_integer
(
    id             bigserial not null,
    data           int       not null,
    effective_date timestamp not null,
    sensor_id      int8      not null,
    constraint data_type_integer_pkey primary key (id),
    constraint data_type_integer_sensor_details_fkey foreign key (sensor_id) references sensor_details (id)
);

create table data_type_double
(
    id             bigserial not null,
    data           double precision not null,
    effective_date timestamp not null,
    sensor_id      int8      not null,
    constraint data_type_double_pkey primary key (id),
    constraint data_type_double_sensor_details_fkey foreign key (sensor_id) references sensor_details (id)

);

create table data_type_boolean
(
    id             bigserial not null,
    data           bool      not null,
    effective_date timestamp not null,
    sensor_id      int8      not null,
    constraint data_type_boolean_pkey primary key (id),
    constraint data_type_boolean_sensor_details_fkey foreign key (sensor_id) references sensor_details (id)

);