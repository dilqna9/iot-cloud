create table sensor_details
(
    id          bigserial not null,
    sensor_name text      not null,
    constraint sensor_details_pkey primary key (id)
);