package com.next.iotcloud.repositoriestests;

import com.next.iotcloud.models.DataTypeString;
import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositories.DataTypeStringRepository;
import com.next.iotcloud.repositories.SensorDetailsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DataTypeStringRepositoryTests {

    @Autowired
    private DataTypeStringRepository dataTypeStringRepository;

    @Autowired
    private SensorDetailsRepository sensorDetailsRepository;


    @Test
    public void testSaveOneStringAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeString dataTypeString = new DataTypeString(1, "SensorTest1", new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeStringRepository.save(dataTypeString);

        List<DataTypeString> dataTypeStringList = dataTypeStringRepository.findAll();
        assertEquals(dataTypeStringList.size(), 1);
    }

    @Test
    public void testSaveTwoStringAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeString dataTypeStringFirst = new DataTypeString(1, "SensorTest1", new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeStringRepository.save(dataTypeStringFirst);
        DataTypeString dataTypeStringSecond = new DataTypeString(2, "SensorTest2", new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeStringRepository.save(dataTypeStringSecond);

        List<DataTypeString> expectedDataTypeStringList = dataTypeStringRepository.findAll();

        assertNotNull(expectedDataTypeStringList);
        assertEquals(expectedDataTypeStringList.size(), 2);
    }
}
