package com.next.iotcloud.repositoriestests;

import com.next.iotcloud.models.DataTypeBoolean;
import com.next.iotcloud.models.DataTypeDouble;
import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositories.DataTypeBooleanRepository;
import com.next.iotcloud.repositories.SensorDetailsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DataTypeBooleanRepositoryTests {

    @Autowired
    private DataTypeBooleanRepository dataTypeBooleanRepository;

    @Autowired
    private SensorDetailsRepository sensorDetailsRepository;


    @Test
    public void testSaveOneBooleanAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeBoolean expectedDataTypeBoolean = new DataTypeBoolean(1, true, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeBooleanRepository.save(expectedDataTypeBoolean);

        List<DataTypeBoolean> actualDataTypeBooleanList = dataTypeBooleanRepository.findAll();

        assertNotNull(actualDataTypeBooleanList);
        assertEquals(actualDataTypeBooleanList.size(), 1);
    }

    @Test
    public void testSaveTwoBooleanAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeBoolean dataTypeBooleanFirst = new DataTypeBoolean(1, false, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeBooleanRepository.save(dataTypeBooleanFirst);
        DataTypeBoolean dataTypeBooleanSecond = new DataTypeBoolean(2, true, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeBooleanRepository.save(dataTypeBooleanSecond);

        List<DataTypeBoolean> expectedDataTypeBooleanList = dataTypeBooleanRepository.findAll();

        assertNotNull(expectedDataTypeBooleanList);
        assertEquals(expectedDataTypeBooleanList.size(), 2);
    }

}
