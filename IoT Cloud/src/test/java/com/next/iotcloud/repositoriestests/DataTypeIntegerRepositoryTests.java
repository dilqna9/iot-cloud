package com.next.iotcloud.repositoriestests;

import com.next.iotcloud.models.*;
import com.next.iotcloud.repositories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DataTypeIntegerRepositoryTests {

    @Autowired
    private DataTypeIntegerRepository dataTypeIntegerRepository;

    @Autowired
    private SensorDetailsRepository sensorDetailsRepository;

    @Autowired
    private DataTypeDoubleRepository dataTypeDoubleRepository;

    @Autowired
    private DataTypeBooleanRepository dataTypeBooleanRepository;

    @Autowired
    private DataTypeStringRepository dataTypeStringRepository;


    @Test
    public void testSaveOneIntegerAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeInteger expectedDataTypeInteger = new DataTypeInteger(1, 166, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeIntegerRepository.save(expectedDataTypeInteger);

        List<DataTypeInteger> actualDataTypeIntegerList = dataTypeIntegerRepository.findAll();

        assertNotNull(actualDataTypeIntegerList);
        assertEquals(actualDataTypeIntegerList.size(), 1);
    }

    @Test
    public void testSaveTwoIntegerAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeInteger dataTypeIntegerFirst = new DataTypeInteger(1, 140, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeIntegerRepository.save(dataTypeIntegerFirst);
        DataTypeInteger dataTypeIntegerSecond = new DataTypeInteger(2, 160, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeIntegerRepository.save(dataTypeIntegerSecond);

        List<DataTypeInteger> expectedDataTypeIntegerList = dataTypeIntegerRepository.findAll();

        assertNotNull(expectedDataTypeIntegerList);
        assertEquals(expectedDataTypeIntegerList.size(), 2);
    }

    @Test
    public void testSaveOneIntegerSaveOneDoubleSaveOneBooleanSaveOneStringAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeInteger dataTypeInteger = new DataTypeInteger(1, 140, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeIntegerRepository.save(dataTypeInteger);
        DataTypeDouble dataTypeDouble = new DataTypeDouble(2, 160.6, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeDoubleRepository.save(dataTypeDouble);
        DataTypeString dataTypeString = new DataTypeString(1, "aaa", new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeStringRepository.save(dataTypeString);
        DataTypeBoolean dataTypeBoolean = new DataTypeBoolean(1, false, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeBooleanRepository.save(dataTypeBoolean);

        List<DataTypeInteger> expectedDataTypeIntegerList = dataTypeIntegerRepository.findAll();
        List<DataTypeDouble> expectedDataTypeDoubleList = dataTypeDoubleRepository.findAll();
        List<DataTypeBoolean> expectedDataTypeBooleanList = dataTypeBooleanRepository.findAll();
        List<DataTypeString> expectedDataTypeStringList = dataTypeStringRepository.findAll();

        assertNotNull(expectedDataTypeIntegerList);
        assertNotNull(expectedDataTypeDoubleList);
        assertNotNull(expectedDataTypeBooleanList);
        assertNotNull(expectedDataTypeStringList);
        assertEquals((expectedDataTypeIntegerList.size() + expectedDataTypeDoubleList.size() + expectedDataTypeBooleanList.size() + expectedDataTypeStringList.size()), 4);
    }

    @Test
    public void testSaveOneIntegerSaveOneDoubleAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeInteger dataTypeInteger = new DataTypeInteger(1, 140, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeIntegerRepository.save(dataTypeInteger);
        DataTypeDouble dataTypeDouble = new DataTypeDouble(2, 160.6, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeDoubleRepository.save(dataTypeDouble);

        List<DataTypeInteger> expectedDataTypeIntegerList = dataTypeIntegerRepository.findAll();
        List<DataTypeDouble> expectedDataTypeDoubleList = dataTypeDoubleRepository.findAll();

        assertNotNull(expectedDataTypeIntegerList);
        assertNotNull(expectedDataTypeDoubleList);
        assertEquals((expectedDataTypeIntegerList.size() + expectedDataTypeDoubleList.size()), 2);
    }
}
