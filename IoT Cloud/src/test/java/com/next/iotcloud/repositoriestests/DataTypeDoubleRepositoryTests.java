package com.next.iotcloud.repositoriestests;

import com.next.iotcloud.models.DataTypeDouble;
import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositories.DataTypeDoubleRepository;
import com.next.iotcloud.repositories.SensorDetailsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DataTypeDoubleRepositoryTests {

    @Autowired
    private DataTypeDoubleRepository dataTypeDoubleRepository;

    @Autowired
    private SensorDetailsRepository sensorDetailsRepository;

    @Test
    public void testSaveOneDoubleAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeDouble expectedDataTypeDouble = new DataTypeDouble(1, 166.6, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeDoubleRepository.save(expectedDataTypeDouble);

        List<DataTypeDouble> actualDataTypeDoubleList = dataTypeDoubleRepository.findAll();

        assertNotNull(actualDataTypeDoubleList);
        assertEquals(actualDataTypeDoubleList.size(), 1);
    }

    @Test
    public void testSaveTwoDoubleAndFindAll() {

        SensorDetails sensorDetails = new SensorDetails(1, "temp");
        sensorDetailsRepository.save(sensorDetails);

        DataTypeDouble dataTypeDoubleFirst = new DataTypeDouble(1, 140.6, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeDoubleRepository.save(dataTypeDoubleFirst);
        DataTypeDouble dataTypeDoubleSecond = new DataTypeDouble(2, 160.6, new Date(), sensorDetailsRepository.findByName("temp"));
        dataTypeDoubleRepository.save(dataTypeDoubleSecond);

        List<DataTypeDouble> expectedDataTypeDoubleList = dataTypeDoubleRepository.findAll();

        assertNotNull(expectedDataTypeDoubleList);
        assertEquals(expectedDataTypeDoubleList.size(), 2);
    }
}
