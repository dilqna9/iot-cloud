package com.next.iotcloud.repositoriestests;

import com.next.iotcloud.models.SensorDetails;
import com.next.iotcloud.repositories.SensorDetailsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SensorDetailsRepositoryTests {

    @Autowired
    private SensorDetailsRepository sensorDetailsRepository;


    @Test
    public void testSaveAndFindByName() {

        SensorDetails sensorDetails = new SensorDetails(1, "abv");
        sensorDetailsRepository.save(sensorDetails);

        SensorDetails expectedSensorDetails = sensorDetailsRepository.findByName("abv");

        assertNotNull(sensorDetails);
        assertEquals(expectedSensorDetails.getName(), sensorDetails.getName());
    }


    @Test
    public void testSaveAndFindAll() {

        SensorDetails sensorDetailsFirst = new SensorDetails(1, "abv");
        sensorDetailsRepository.save(sensorDetailsFirst);
        SensorDetails sensorDetailsSecond = new SensorDetails(2, "def");
        sensorDetailsRepository.save(sensorDetailsSecond);

        List<SensorDetails> allSensorDetails = new ArrayList<>();
        allSensorDetails.add(sensorDetailsFirst);
        allSensorDetails.add(sensorDetailsSecond);

        List<SensorDetails> allSensorDetailsRepository = sensorDetailsRepository.findAll();

        assertNotNull(allSensorDetailsRepository);
        assertEquals(allSensorDetails.size(), allSensorDetailsRepository.size());
    }

}
