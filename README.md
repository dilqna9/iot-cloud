# IOT Cloud
*by Dilyana Dimova and Miroslav Balimezov*  

## How To Run
To run the application you should:

1. Clone the project Git repository.
2. Create database with name "sensors_db".
3. Go to File -> Settings -> Plugins /Marketplace tab/ in your IntelliJ IDEA and install EnvFile plugin or add EnvFile plugin from [EnvFile Plugin](https://plugins.jetbrains.com/plugin/7861-envfile).
4. Edit the DB url, username and password fields in env_properties file according to your configuration.
5. Go to Run -> Edit Configurations -> EnvFile in your IntelliJ IDEA and add env_properties file.
6. Build and run the applicataion.
7. Run the DB script with name "sensor_db.sql" to fill data in the tables.


The application will be available [here](http://localhost:8080).

The REST part of the appliacation is documented with Swagger and is available [here](http://localhost:8080/swagger-ui.html) while the application is running.