insert into sensor_details(id, sensor_name)
values (1, 'A');
insert into sensor_details(id, sensor_name)
values (2, 'B');
insert into sensor_details(id, sensor_name)
values (3, 'C');

insert into data_type_integer(id, data, sensor_id, effective_date)
values (1, 134, 3, current_timestamp);
insert into data_type_integer(id, data, sensor_id, effective_date)
values (2, 50, 1, current_timestamp);
insert into data_type_integer(id, data, sensor_id, effective_date)
values (3, 250, 2, current_timestamp);

insert into data_type_double(id, data, sensor_id, effective_date)
values (1, 110.5, 1, current_timestamp);
insert into data_type_double(id, data, sensor_id, effective_date)
values (2, 55.9, 2, current_timestamp);
insert into data_type_double(id, data, sensor_id, effective_date)
values (3, 50, 3, current_timestamp);

insert into data_type_string(id, data, sensor_id, effective_date)
values (1, 'wa', 1, current_timestamp);
insert into data_type_string(id, data, sensor_id, effective_date)
values (2, 'va', 3, current_timestamp);
insert into data_type_string(id, data, sensor_id, effective_date)
values (3, 'za', 2, current_timestamp);

insert into data_type_boolean(id, data, sensor_id, effective_date)
values (1, true, 1, current_timestamp);
insert into data_type_boolean(id, data, sensor_id, effective_date)
values (2, false, 1, current_timestamp);
insert into data_type_boolean(id, data, sensor_id, effective_date)
values (3, false, 3, current_timestamp);